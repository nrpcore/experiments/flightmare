from nrp_core import *
from nrp_core.data.nrp_protobuf import GazeboSetVelocityDataPack
@TransceiverFunction("gazebo")
def transceiver_function():

    vel_rotor0   = GazeboSetVelocityDataPack("hummingbird_velocity::hummingbird_rotor_0_joint", "gazebo")
    vel_rotor1   = GazeboSetVelocityDataPack("hummingbird_velocity::hummingbird_rotor_1_joint", "gazebo")
    vel_rotor2   = GazeboSetVelocityDataPack("hummingbird_velocity::hummingbird_rotor_2_joint", "gazebo")
    vel_rotor3   = GazeboSetVelocityDataPack("hummingbird_velocity::hummingbird_rotor_3_joint", "gazebo")
    
    vel_rotor0.data.velocity = 100
    vel_rotor1.data.velocity = 100   
    vel_rotor2.data.velocity = 100   
    vel_rotor3.data.velocity = 100   

    # print("Left voltage:  " + str(left_voltage))
    # print("Right voltage: " + str(right_voltage))
    # print("Forward velocity: " + str(forward_vel))
    # print("Rotational vel:   " + str(rot_vel))

    # print(vel_rotor3)
    # return []
    return [vel_rotor0, vel_rotor1, vel_rotor2, vel_rotor3]

