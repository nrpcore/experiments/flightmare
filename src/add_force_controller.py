from nrp_core import *
from nrp_core.data.nrp_protobuf import GazeboSetRelativeForceDataPack
import numpy as np
from PIL import Image
import time
import cv2
@EngineDataPack(keyword='rotor_0', id=DataPackIdentifier("hummingbird_force::hummingbird_rotor_0", "gazebo"))
@EngineDataPack(keyword='camera', id=DataPackIdentifier('flightmare::camera', 'gazebo'))
@TransceiverFunction("gazebo")
def transceiver_function(camera,rotor_0):

    force_link0   = GazeboSetRelativeForceDataPack("hummingbird_force::hummingbird_rotor_0", "gazebo")
    force_link1   = GazeboSetRelativeForceDataPack("hummingbird_force::hummingbird_rotor_1", "gazebo")
    force_link2   = GazeboSetRelativeForceDataPack("hummingbird_force::hummingbird_rotor_2", "gazebo")
    force_link3   = GazeboSetRelativeForceDataPack("hummingbird_force::hummingbird_rotor_3", "gazebo")

    # print(type(camera))

    if not camera.isEmpty():

        # Set to True to display camera image data and pause for 10 s
        show_image = True
        if show_image:
            d = np.frombuffer(camera.data.imageData, np.uint8)
            cv_image = d.reshape((camera.data.imageHeight, camera.data.imageWidth, 3))
            img = Image.fromarray(cv_image)
            img.save("/home/nrpuser/nrp-core/examples/flightmare_gazebo_json_based_example/images/"+ str(int(time.time())) + ".png")
            img.show()
            # time.sleep(10)
    
    force_link0.data.x = 0
    force_link0.data.y = 0
    force_link0.data.z = 20
    
    force_link1.data.x = 0
    force_link1.data.y = 0
    force_link1.data.z = 20

    force_link2.data.x = 0
    force_link2.data.y = 0
    force_link2.data.z = 20

    force_link3.data.x = 0
    force_link3.data.y = 0
    force_link3.data.z = 20

    print(rotor_0.data)

    # print((rotor_0.data.position[0]))
    # print((rotor_0.data.position[1]))
    # print((rotor_0.data.position[2]))

    # print(force_link3)
    # return []
    return [force_link0,force_link1,force_link2,force_link3]

