
from nrp_core import *
from nrp_core.data.nrp_json import *
from nrp_core.data.nrp_protobuf import *


@EngineDataPack(keyword='rotor_0', id=DataPackIdentifier("hummingbird_force::hummingbird_rotor_0", "gazebo"))
@TransceiverFunction("unity")
def transceiver_function(rotor_0):

    joint = UnitySimPositionDataPack("position", "unity")

     # print((rotor_0.data.position[0]))
    # print((rotor_0.data.position[1]))
    # print((rotor_0.data.position[2]))
    joint.data.x = rotor_0.data.position[0]
    joint.data.y = rotor_0.data.position[2]
    joint.data.z = rotor_0.data.position[1]
    print(joint)

    return [joint]

