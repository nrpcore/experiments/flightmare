### Start the experiment

Terminal #1
```
NRPCoreSim -c config/simulation_config.json
```
![unity_gazebo_image](https://imgur.com/mYuO33b.png)

Terminal #2
```
gzclient
```